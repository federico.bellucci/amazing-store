Amazing Store is a sample application built with Spring Boot for the back end and React for the fron tend.
It consumes a product API at [localhost:8081](http://localhost:8081) and provides a new API at [localhost:8080/api](http://localhost:8080/api) which is in turn consumed by the React application at [localhost:8080](http://localhost:8080)

# Requirements
- Java 1.8
- Maven
- product API running at [localhost:8081](http://localhost:8081)

# Build and execution
To run the application run on the command line:
`$ mvn spring-boot:run`

Alternatively you can create a JAR file and execute it:
`$ mvn package -DskipTests`
`$ java -jar target/amazing-store-1.0.0.jar`

It will download the required libraries and start a web server on the default url [localhost:8080](http://localhost:8080)

# API
The application also provides the following API at [localhost:8080/api](http://localhost:8080/api)
- `GET /products4`
- `GET /products/<product-id>`
- `GET /products/checkout?ids=<comma-separated-product-ids>`
