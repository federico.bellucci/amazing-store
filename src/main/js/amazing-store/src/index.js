import React from 'react'
import ReactDOM from 'react-dom'
import {Provider} from 'react-redux'
import storeFactory from './store'
import 'bootstrap/dist/css/bootstrap.css'
import './index.css'
import App from './App'
//import registerServiceWorker from './registerServiceWorker'

ReactDOM.render(
    <Provider store={storeFactory()}>
        <App />
    </Provider>, 
    document.getElementById('root')
)
//registerServiceWorker()
