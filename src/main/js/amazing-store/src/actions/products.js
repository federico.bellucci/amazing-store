export const SET_PRODUCTS = 'SET_PRODUCTS'

export const setProducts = (items) => ({
    type: SET_PRODUCTS,
    items
})