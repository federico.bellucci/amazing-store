export const ADD_TO_CART = 'ADD_TO_CART'
export const EMPTY_CART = 'EMPTY_CART_CART'
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART'
export const CHECKOUT = 'CHECKOUT'
export const RESET_CHECKOUT = 'RESET_CHECKOUT'


export const addToCart = (item, quantity) => {
    return {
        type: ADD_TO_CART,
        item: item,
        quantity: quantity
    }
}

export const emptyCart = () => {
    return {
        type: EMPTY_CART
    }

}

export const removeFromCart = (index) => {
    return {
        type: REMOVE_FROM_CART,
        index: index
    }
}

export const checkout = (checkout) => {
    return {
        type: CHECKOUT,
        checkout: checkout
    }
}

export const resetCheckout = () => {
    return {
        type: RESET_CHECKOUT
    }
}