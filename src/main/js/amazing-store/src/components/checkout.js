import React, { Component } from 'react'
import {connect} from 'react-redux'
import {resetCheckout, emptyCart} from '../actions/cart'
import { getPriceString } from '../utils/utils'

export class Checkout extends Component {

    constructor () {
        super()

        this.restart = this.restart.bind(this)
        this.backToCart = this.backToCart.bind(this)
    }

    restart () {
        this.props.resetCheckout()
        this.props.emptyCart()
    }

    backToCart () {
        this.props.resetCheckout()
    }

    renderCheckoutRow (value, label) {
        return <div className="row">
            <div className="col-sm-6 text-right"><strong>{label}</strong>: </div>
            <div className="col-sm-6">{getPriceString(value)}</div>
        </div>
    }

    render () {
        let {total, promo, payable} = this.props.checkout
        return (
            <div className="cart row mt-3">
                <div className="col-sm-8 offset-sm-2">
                    <div className="card">
                        <div className="card-header text-center"><h4>Checkout</h4></div>
                        <div className="card-body">
                            {this.renderCheckoutRow(total, 'Total')}
                            {this.renderCheckoutRow(promo, 'Promotions')}
                            {this.renderCheckoutRow(payable, 'Payable')}
                        </div>
                        <div className="card-footer text-center">
                            <button type="button" className="btn btn-primary mr-2" 
                                onClick={this.restart}>
                                    New Shopping cart
                            </button>
                            <button type="button" className="btn btn-secondary" 
                                onClick={this.backToCart}>
                                    Back to cart
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        resetCheckout: () => dispatch(resetCheckout()),
        emptyCart: () => dispatch(emptyCart()),
    }
}

export default connect(null, mapDispatchToProps)(Checkout)
