import React, { Component } from 'react'
import {connect} from 'react-redux'
import 'whatwg-fetch'
import 'promise'
import {addToCart} from '../actions/cart'
import Product from './product'

export class Products extends Component {

    render () {
        return <div className="card-deck mt-3">
            {
                this.props.products.map(product => 
                    <Product product={product} key={product.id} addToCart={this.props.addToCart}/>
                )
            }
        </div>
    }
}

const mapDispatchToProps = dispatch => {
    return {
        addToCart: (item, quantity) => dispatch(addToCart(item, quantity)),
    }
}

export default connect(null, mapDispatchToProps)(Products)
