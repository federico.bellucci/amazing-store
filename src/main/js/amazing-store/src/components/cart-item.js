import React, { Component } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {getPriceString} from '../utils/utils'

export default class CartItem extends Component{

    constructor () {
        super()

        this.deleteItem = this.deleteItem.bind(this)
    }

    deleteItem () {
        this.props.deleteItem(this.props.index)
    }

    renderPromotions (promotions) {
        return promotions.map(promotion => promotion.name + '!').join(' - ')
    }

    render () {
        let {name, price, promotions} = this.props.item

        return (
            <tr className="cart-item">
                <td>
                    <span className="product-name"><strong>{name}</strong></span>
                </td>
                <td>
                    <span>{getPriceString(price)}</span>
                </td>
                <td>
                    <span>{this.renderPromotions(promotions)}</span>
                </td>
                <td>
                    <button type="button" className="btn btn-link btn-sm" onClick={this.deleteItem} hidden={this.props.readOnly}>
                        <FontAwesomeIcon icon="trash" />
                    </button>
                </td>
            </tr>
        )
    }
}