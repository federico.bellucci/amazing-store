import React, { Component } from 'react'
import {connect} from 'react-redux'
import 'whatwg-fetch'
import 'promise'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import CartItem from './cart-item'
import {emptyCart, removeFromCart, checkout} from '../actions/cart'

import {PRODUCT_API} from '../App'

export class Cart extends Component {

    constructor () {
        super() 

        this.emptyCart = this.emptyCart.bind(this)
        this.doCheckout = this.doCheckout.bind(this)
    }

    emptyCart(e) {
        this.props.emptyCart()
  	}

    doCheckout () {
        let ids = this.props.items.map(item => item.id)
        fetch(PRODUCT_API + '/checkout?ids=' + ids)
            .then(res => res.json())
            .then(json => {
                this.props.doCheckout(json)
            })
            .catch(err => console.error(err))
    }

    render () {
        let checkingOut = this.props.checkout != null

        return (
            <div className="cart row mt-3">
                <div className="col-sm-8 offset-sm-2">
                    <div className="card">
                        <div className="card-header">
                            <h3 className="text-center"><FontAwesomeIcon icon="shopping-cart" /> Shopping Cart</h3>
                        </div>
                        {
                            this.props.items.length ? 
                                <div className="table-container table-container-scrolled">
                                    <table className="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Product</th>
                                                <th>Price</th>
                                                <th>Promotions</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                this.props.items.map((item, index) => 
                                                    <CartItem item={item} key={index} index={index} deleteItem={this.props.removeFromCart} readOnly={checkingOut}/>)
                                            }
                                        </tbody>
                                    </table>
                                </div>
                                : <div className="card-body"><div className="alert alert-info">Cart is empty</div></div>
                        }
                        
                        <div className="card-footer text-center" hidden={checkingOut}>
                            <button type="button" className="btn btn-secondary mr-2" 
                                onClick={this.emptyCart} disabled={!this.props.items.length}>
                        Empty cart
                            </button>
                            <button type="button" className="btn btn-primary" 
                                onClick={this.doCheckout} disabled={!this.props.items.length} >
                        Checkout
                            </button>
                            
                        </div>
                    </div>
                </div>
            </div>
            
        )
    }
}

const mapStateToProps = state => {
    return {
        items: state.cart.items,
        checkout: state.cart.checkout
    }
}


const mapDispatchToProps = dispatch => {
    return {
        removeFromCart: (index) => dispatch(removeFromCart(index)),
        emptyCart: () => dispatch(emptyCart()),
        doCheckout: (checkoutResult) => dispatch(checkout(checkoutResult)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Cart)
