import React, { Component } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { getPriceString } from '../utils/utils'

export default class Product extends Component {

    constructor () {
        super()

        this.state = {
            quantity: 1
        }

        this.updateQuantity = this.updateQuantity.bind(this)
        this.addToCart = this.addToCart.bind(this)
    }

    updateQuantity (e) {
        this.setState({
            quantity: e.target.value
        })
    }

    addToCart () {
        this.props.addToCart(this.props.product, this.state.quantity)
    }

    renderPrice (price) {
        return <span>{getPriceString(price)}</span>
    }

    renderPromotions (promotions) {
        return promotions.map(promotion => promotion.name + '!').join(' - ')
    }


    render () {
        let {name, price, promotions} = this.props.product,
            renderedPromos = this.renderPromotions(promotions) || 'No promotions yet'


        return (
            <div className="card" style={{width: '250px', margin: '5px'}}>
                <img className="card-img-top" src="http://placehold.it/250px150/" alt={name}/>
                <div className="card-body">
                    <h5 className="card-title text-center">{name}</h5>
                    <p className="card-text text-right" >{this.renderPrice(price)}</p>
                    <p className={`card-text text-center alert ${promotions.length ? 'alert-danger' : ''}`}>{renderedPromos}</p>
                </div>                    
                <div className="card-footer input-group">
                    <input type="text" className="form-control" value={this.state.quantity} onChange={this.updateQuantity} 
                        placeholder="Quantity" aria-label="Quantity" aria-describedby="button-addon2"/>
                    <div className="input-group-append">
                        <button className="btn btn-primary" type="button" onClick={this.addToCart} id="button-addon2">
                            <FontAwesomeIcon icon="shopping-cart" /> Add to cart
                        </button>
                    </div>
                </div>
            </div>
        )
    }
}