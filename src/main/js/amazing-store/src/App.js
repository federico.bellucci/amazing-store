import React, { Component } from 'react'
import {connect} from 'react-redux'
import './App.css'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faTrash, faShoppingCart } from '@fortawesome/free-solid-svg-icons'
import Products from './components/products'
import Cart from './components/cart'
import Checkout from './components/checkout'

library.add(faTrash, faShoppingCart)

export const PRODUCT_API = 'http://localhost:8080/api/products'

class App extends Component {

    constructor () {
        super()

        this.state = {
            products: []
        }
    }

    componentDidMount () {
        this.loadAllProducts()
    }
   

    loadAllProducts () {
        fetch(PRODUCT_API)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    products: json
                })
            })
            .catch(err => console.error(err))
    }

    render() {
        return (
        	<div>
        	<h1 className="text-center p-3 bg-dark text-light">Amazing Shop</h1>
                <div className="container">

                    <Cart/>
                    {
                	this.props.checkout 
                		? <Checkout checkout={this.props.checkout} />
                		: <Products products={this.state.products}/>
                    }
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        checkout: state.cart.checkout
    }
}

export default connect(mapStateToProps, null)(App)

