import {ADD_TO_CART, EMPTY_CART, REMOVE_FROM_CART, CHECKOUT, RESET_CHECKOUT} from '../actions/cart'

const intialState = {
    items: [], 
    checkout: null
}

const cart = (state = intialState, action) => {
    switch (action.type) {
    case ADD_TO_CART: 
        let addedItems = []
        for (let i=0; i<action.quantity; i++) {
            addedItems.push(action.item)
        }
    	return {
        	...state, 
        	items: [...state.items, ...addedItems]
        }
    case EMPTY_CART: 
        return {
            ...state,
            items: []
        }
    case REMOVE_FROM_CART: 
        let newItems = [...state.items]
        newItems.splice(action.index, 1)
        return {
            ...state,
            items: newItems
        }
    case CHECKOUT: 
        return {
            ...state,
            checkout: action.checkout
        }
    case RESET_CHECKOUT: 
        return {
            ...state,
            checkout: null
        }
    default:
        return state
    }
}

export default cart