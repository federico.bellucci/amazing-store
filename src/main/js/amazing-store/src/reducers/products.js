import {SET_PRODUCTS} from '../actions/products'

const intialState = []

const products = (state = intialState, action) => {
    switch (action.type) {
    case SET_PRODUCTS: 
    	return action.items
    default:
        return state
    }
}

export default products