package com.amazingstore.amazingstore.utils;

public class Utils {

	public static String getPriceString(int price) {
		return String.format("£%d.%02d", price / 100, price % 100);
	}

}
