package com.amazingstore.amazingstore.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.amazingstore.amazingstore.client.ProductPromotionTemplate;
import com.amazingstore.amazingstore.model.product.Product;
import com.amazingstore.amazingstore.model.product.ProductAdapter;
import com.amazingstore.amazingstore.service.ProductServiceInterface;
import com.amazingstore.amazingstore.service.ProductTemplateServiceInterface;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/products")
public class ProductRestController {

	@Autowired
	ProductServiceInterface productService;

	@Autowired
	ProductTemplateServiceInterface productTemplateService;

	@Autowired
	ProductAdapter<ProductPromotionTemplate> adapter;

	@GetMapping
	List<Product> getProducts() {
		List<ProductPromotionTemplate> productPromotionTemplates = this.productTemplateService
				.getProductPromotionTemplates();
		List<Product> products = new ArrayList<Product>();
		for (ProductPromotionTemplate productPromotionTemplate : productPromotionTemplates) {
			products.add(adapter.getProduct(productPromotionTemplate));
		}
		return products;
	}

	@GetMapping("/{productId}")
	Product getProduct(@PathVariable String productId) {
		ProductPromotionTemplate productPromotionTemplate = this.productTemplateService
				.getProductPromotionTemplate(productId);
		return adapter.getProduct(productPromotionTemplate);
	}

}
