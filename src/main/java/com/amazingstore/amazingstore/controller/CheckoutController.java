package com.amazingstore.amazingstore.controller;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.amazingstore.amazingstore.client.ProductPromotionTemplate;
import com.amazingstore.amazingstore.model.checkout.Checkout;
import com.amazingstore.amazingstore.model.product.Product;
import com.amazingstore.amazingstore.model.product.ProductAdapter;
import com.amazingstore.amazingstore.service.ProductServiceInterface;
import com.amazingstore.amazingstore.service.ProductTemplateServiceInterface;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/products/checkout")
public class CheckoutController {

	@Autowired
	ProductServiceInterface productService;
	
	@Autowired
	ProductTemplateServiceInterface productTemplateService;
	
	@Autowired
	ProductAdapter<ProductPromotionTemplate> adapter;

	@GetMapping
	Checkout checkout(@RequestParam String ids) throws Exception {

		String[] productIds = ids.split(",");

		Set<String> idSet = new HashSet<String>();
		Collections.addAll(idSet, productIds);

		Set<Product> productSet = new HashSet<Product>();
		for (String productId : idSet) {
			ProductPromotionTemplate productTemplate = productTemplateService.getProductPromotionTemplate(productId);
			productSet.add(this.adapter.getProduct(productTemplate));
		}

		return this.productService.checkout(productSet, productIds);
	}

}
