package com.amazingstore.amazingstore.controller;

import java.util.HashMap;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;

import com.amazingstore.amazingstore.model.checkout.MalfermodPromotionException;

@ControllerAdvice
public class RestResponseEntityExceptionHandler {

	@SuppressWarnings("serial")
	@ExceptionHandler(MalfermodPromotionException.class)
	protected ResponseEntity<Object> handleMalformedPromotion(MalfermodPromotionException ex) {

		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new HashMap<String, String>() {
			{
				put("message", "A promotion used in the basket has invalid data");
			}
		});
	}
	
	@SuppressWarnings("serial")
	@ExceptionHandler(HttpClientErrorException.class)
	protected ResponseEntity<Object> handleClientException(HttpClientErrorException ex) {

		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new HashMap<String, String>() {
			{
				put("message", "Product not found");
			}
		});
	}
	
	@SuppressWarnings("serial")
	@ExceptionHandler(RestClientException.class)
	protected ResponseEntity<Object> handleRestClientException(RestClientException ex) {

		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new HashMap<String, String>() {
			{
				put("message", "Product not found " + ex.getMessage());
			}
		});
	}
}
