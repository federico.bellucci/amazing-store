package com.amazingstore.amazingstore.client;

import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductPromotionTemplate {
	protected String id;
	protected String name;
	protected int price;
	protected Set<Map<String, String>> promotions;

	protected ProductPromotionTemplate() {}

	public ProductPromotionTemplate(String id, String name, int price, Set<Map<String, String>> promotions) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
		this.promotions = promotions;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the price
	 */
	public int getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(int price) {
		this.price = price;
	}

	/**
	 * @return the promotions
	 */
	public Set<Map<String, String>> getPromotions() {
		return promotions;
	}

	/**
	 * @param promotions the promotions to set
	 */
	public void setPromotions(Set<Map<String, String>> promotions) {
		this.promotions = promotions;
	}
}
