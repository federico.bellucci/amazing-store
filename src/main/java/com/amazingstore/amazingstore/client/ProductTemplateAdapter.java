package com.amazingstore.amazingstore.client;

import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.amazingstore.amazingstore.model.product.Product;
import com.amazingstore.amazingstore.model.product.ProductAdapter;
import com.amazingstore.amazingstore.model.promotion.AbstractPromotionFactory;
import com.amazingstore.amazingstore.model.promotion.BuyXGetYFree;
import com.amazingstore.amazingstore.model.promotion.BuyXGetYFreeFactory;
import com.amazingstore.amazingstore.model.promotion.FlatPercent;
import com.amazingstore.amazingstore.model.promotion.FlatPercentFactory;
import com.amazingstore.amazingstore.model.promotion.Promotion;
import com.amazingstore.amazingstore.model.promotion.QuantityBasedPriceOverride;
import com.amazingstore.amazingstore.model.promotion.QuantityBasedPriceOverrideFactory;

@Component
public class ProductTemplateAdapter implements ProductAdapter<ProductPromotionTemplate> {

	@Override
	public Product getProduct(ProductPromotionTemplate productTemplate) {
		Set<Map<String, String>> promotionMaps = productTemplate.getPromotions();
		Set<Promotion> promotions = promotionMaps.stream().map(new Function<Map<String, String>, Promotion>() {
			@Override
			public Promotion apply(Map<String, String> attributes) {
				AbstractPromotionFactory factory;
				switch (attributes.get(AbstractPromotionFactory.FIELD_TYPE)) {
				case BuyXGetYFree.TYPE:
					factory = new BuyXGetYFreeFactory();
					break;
				case FlatPercent.TYPE:
					factory = new FlatPercentFactory();
					break;
				case QuantityBasedPriceOverride.TYPE:
					factory = new QuantityBasedPriceOverrideFactory();
					break;
				default:
					// TODO
					return null;
				}
				return factory.createPromotion(attributes);
			}
		}).collect(Collectors.toSet());

		return new Product(productTemplate.getId(), productTemplate.getName(), productTemplate.getPrice(), promotions);
	}

}
