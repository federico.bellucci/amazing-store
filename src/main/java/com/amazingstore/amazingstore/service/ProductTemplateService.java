package com.amazingstore.amazingstore.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.amazingstore.amazingstore.client.ProductPromotionTemplate;
import com.amazingstore.amazingstore.client.ProductTemplate;
import com.amazingstore.amazingstore.service.error.RestTemplateResponseErrorHandler;

@Service
public class ProductTemplateService implements ProductTemplateServiceInterface {

	private RestTemplate restTemplate;

	@Autowired
	public ProductTemplateService(RestTemplateBuilder restTemplateBuilder) {
		restTemplate = restTemplateBuilder.errorHandler(new RestTemplateResponseErrorHandler()).build();
//		restTemplate = restTemplateBuilder.build();
	}

	public List<ProductTemplate> getProductTemplates() {
		ParameterizedTypeReference<List<ProductTemplate>> listOfProducts = new ParameterizedTypeReference<List<ProductTemplate>>() {
		};

		ResponseEntity<List<ProductTemplate>> response = restTemplate.exchange(PRODUCT_API, HttpMethod.GET, null,
				listOfProducts);

		return response.getBody();
	}

	public List<ProductPromotionTemplate> getProductPromotionTemplates() {
		List<ProductTemplate> productTemplates = this.getProductTemplates();

		List<String> ids = new ArrayList<String>();
		for (ProductTemplate productsTemplate : productTemplates) {
			ids.add(productsTemplate.getId());
		}

		return this.getProductPromotionTemplates(ids);
	}

	public List<ProductPromotionTemplate> getProductPromotionTemplates(List<String> ids) {
		List<ProductPromotionTemplate> products = new ArrayList<>();
		for (String id : ids) {
			products.add(this.getProductPromotionTemplate(id));
		}
		return products;
	}

	public ProductPromotionTemplate getProductPromotionTemplate(String productId) {
		return restTemplate.getForObject(PRODUCT_API + "/" + productId, ProductPromotionTemplate.class);
	}

}
