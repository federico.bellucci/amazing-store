package com.amazingstore.amazingstore.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazingstore.amazingstore.model.checkout.Checkout;
import com.amazingstore.amazingstore.model.checkout.CheckoutStrategyFactory;
import com.amazingstore.amazingstore.model.checkout.PlainCheckout;
import com.amazingstore.amazingstore.model.product.Product;
import com.amazingstore.amazingstore.model.promotion.NoPromotion;
import com.amazingstore.amazingstore.model.promotion.Promotion;

@Service
public class ProductService implements ProductServiceInterface {

	@Autowired
	protected CheckoutStrategyFactory checkoutStrategyFactory;

	public Checkout checkout(Set<Product> productSet, String[] chekoutProductIds) throws Exception {
		Map<String, Product> productMap = new HashMap<String, Product>();
		for (Product product : productSet) {
			if (!productMap.containsKey(product.getId())) {
				productMap.put(product.getId(), product);
			}
		}

		Map<String, Integer> productAmounts = new HashMap<String, Integer>();
		for (String id : chekoutProductIds) {
			if (!productAmounts.containsKey(id)) {
				productAmounts.put(id, 1);
			} else {
				productAmounts.put(id, productAmounts.get(id) + 1);
			}
		}

		Checkout total = new Checkout();
		for (Map.Entry<String, Integer> productAmount : productAmounts.entrySet()) {
			String productId = productAmount.getKey();
			Checkout checkout = this.getSingleProductCheckout(productMap.get(productId), productAmount.getValue());
			total.sum(checkout);
		}

		return total;
	}

	/**
	 * Get the single product checkout with max discount among the promotions
	 * @param product
	 * @param amount
	 * @return
	 * @throws Exception
	 */
	Checkout getSingleProductCheckout(Product product, int amount) throws Exception {
		if (amount <= 0) {
			return new Checkout();
		}

		if (product == null) {
			throw new NullPointerException("Product can't be null");
		}

		int maxPromo = 0;
		Checkout bestPromotionCheckout = new PlainCheckout().checkout(new NoPromotion(), product.getPrice(), amount);
		for (Promotion promotion : product.getPromotions()) {
			Checkout promotionCheckout = checkoutStrategyFactory.createStrategy(promotion).checkout(promotion,
					product.getPrice(), amount);
			if (maxPromo < promotionCheckout.getPromo()) {
				maxPromo = promotionCheckout.getPromo();
				bestPromotionCheckout = promotionCheckout;
			}
		}

		return bestPromotionCheckout;
	}

}
