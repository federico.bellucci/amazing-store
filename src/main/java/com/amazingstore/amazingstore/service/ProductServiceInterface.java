package com.amazingstore.amazingstore.service;

import java.util.Set;

import org.springframework.stereotype.Service;

import com.amazingstore.amazingstore.model.checkout.Checkout;
import com.amazingstore.amazingstore.model.product.Product;

@Service
public interface ProductServiceInterface {
	
	public Checkout checkout (Set<Product> productSet, String[] ids) throws Exception;
	
}
