package com.amazingstore.amazingstore.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.amazingstore.amazingstore.client.ProductPromotionTemplate;
import com.amazingstore.amazingstore.client.ProductTemplate;

@Service
public interface ProductTemplateServiceInterface {

	public static final String PRODUCT_API = "http://localhost:8081/products";

	public List<ProductTemplate> getProductTemplates();

	public List<ProductPromotionTemplate> getProductPromotionTemplates();

	public List<ProductPromotionTemplate> getProductPromotionTemplates(List<String> ids);

	public ProductPromotionTemplate getProductPromotionTemplate(String productId);

}
