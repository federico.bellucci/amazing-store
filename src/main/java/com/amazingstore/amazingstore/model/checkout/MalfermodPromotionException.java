package com.amazingstore.amazingstore.model.checkout;

public class MalfermodPromotionException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -153290349077650103L;

	public MalfermodPromotionException(String message) {
        super(message);
    }

}
