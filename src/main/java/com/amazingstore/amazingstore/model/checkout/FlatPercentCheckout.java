package com.amazingstore.amazingstore.model.checkout;

import com.amazingstore.amazingstore.model.promotion.FlatPercent;
import com.amazingstore.amazingstore.model.promotion.Promotion;

public class FlatPercentCheckout implements CheckoutStrategy {
	
	public FlatPercentCheckout () {
		
	}

	public Checkout checkout(Promotion promotion, int price, int amount) throws MalfermodPromotionException {
		FlatPercent percentPromotion = (FlatPercent) promotion;

		int percent = percentPromotion.getAmount();

		if (0 > percent || percent > 100) {
			throw new MalfermodPromotionException("Percent "+percent+" not valid: amount must be between 0 and 100");
		}

		int total = price * amount;
		int promo = total * (percent)/100;
		int payable = total - promo;
		return new Checkout(total, promo, payable);
	}
}
