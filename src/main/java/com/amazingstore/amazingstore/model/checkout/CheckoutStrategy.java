package com.amazingstore.amazingstore.model.checkout;

import com.amazingstore.amazingstore.model.promotion.Promotion;

public interface CheckoutStrategy {

	Checkout checkout(Promotion promotion, int price, int amount) throws MalfermodPromotionException;
}
