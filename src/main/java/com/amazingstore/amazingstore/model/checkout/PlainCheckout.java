package com.amazingstore.amazingstore.model.checkout;

import com.amazingstore.amazingstore.model.promotion.Promotion;

public class PlainCheckout implements CheckoutStrategy {

	public Checkout checkout(Promotion promotion, int price, int amount) {
		int total = price * amount;
		return new Checkout(total, 0, total);
	}
}
