package com.amazingstore.amazingstore.model.checkout;

import com.amazingstore.amazingstore.model.promotion.Promotion;
import com.amazingstore.amazingstore.model.promotion.QuantityBasedPriceOverride;

public class QuantityBasedPriceOverrideCheckout implements CheckoutStrategy {

	public QuantityBasedPriceOverrideCheckout() {

	}

	public Checkout checkout(Promotion promotion, int price, int amount) throws MalfermodPromotionException {
		QuantityBasedPriceOverride overrirdePromotion = (QuantityBasedPriceOverride) promotion;

		int requiredQuantity = overrirdePromotion.getRequiredQuantity();
		int overridePrice = overrirdePromotion.getPrice();

		if (0 >= requiredQuantity) {
			throw new MalfermodPromotionException("Required quantity must be greater than 0");
		}
		if (0 > overridePrice) {
			throw new MalfermodPromotionException("Override price must be greater than or equal to 0");
		}

		int total = price * amount;
		int payable = (amount / requiredQuantity) * overridePrice + (amount % requiredQuantity) * price;
		int promo = total - payable;

		return new Checkout(total, promo, payable);
	}
}
