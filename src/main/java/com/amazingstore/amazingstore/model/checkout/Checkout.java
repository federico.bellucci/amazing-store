package com.amazingstore.amazingstore.model.checkout;

public class Checkout {
	protected int total;
	protected int promo;
	protected int payable;

	public Checkout() {

	}

	public Checkout(int total, int promo, int payable) {
		super();
		this.total = total;
		this.promo = promo;
		this.payable = payable;
	}

	public void sum(Checkout checkout) {
		this.total += checkout.total;
		this.promo += checkout.promo;
		this.payable += checkout.payable;
	}

	/**
	 * @return the total
	 */
	public int getTotal() {
		return total;
	}

	/**
	 * @param total the total to set
	 */
	public void setTotal(int total) {
		this.total = total;
	}

	/**
	 * @return the promo
	 */
	public int getPromo() {
		return promo;
	}

	/**
	 * @param promo the promo to set
	 */
	public void setPromo(int promo) {
		this.promo = promo;
	}

	/**
	 * @return the payable
	 */
	public int getPayable() {
		return payable;
	}

	/**
	 * @param payable the payable to set
	 */
	public void setPayable(int payable) {
		this.payable = payable;
	}

}
