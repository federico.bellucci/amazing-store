package com.amazingstore.amazingstore.model.checkout;

import com.amazingstore.amazingstore.model.promotion.BuyXGetYFree;
import com.amazingstore.amazingstore.model.promotion.Promotion;

public class BuyXGetYFreeCheckout implements CheckoutStrategy {
	
	public BuyXGetYFreeCheckout () {
		
	}

	public Checkout checkout(Promotion promotion, int price, int amount) throws MalfermodPromotionException {
		BuyXGetYFree buyXGetFree = (BuyXGetYFree) promotion;

		int freeQuantity = buyXGetFree.getFreeQuantity();
		int requiredQuantity = buyXGetFree.getRequiredQuantity();

		if (requiredQuantity <= 0) {
			throw new MalfermodPromotionException("Required quantity must be greater than 0");
		}
		if (freeQuantity < 0) {
			throw new MalfermodPromotionException("Required quantity must be greater than or equal to 0");
		}
		if (freeQuantity > requiredQuantity) {
			throw new MalfermodPromotionException("Required quantity must be greater than or equal to free quantity");
		}

		int freeProducts = amount / requiredQuantity * freeQuantity;
		int total = price * amount;
		int promo = price * freeProducts;
		int payable = total - promo;
		return new Checkout(total, promo, payable);
	}
}
