package com.amazingstore.amazingstore.model.checkout;

import org.springframework.stereotype.Component;

import com.amazingstore.amazingstore.model.promotion.BuyXGetYFree;
import com.amazingstore.amazingstore.model.promotion.FlatPercent;
import com.amazingstore.amazingstore.model.promotion.Promotion;
import com.amazingstore.amazingstore.model.promotion.QuantityBasedPriceOverride;

@Component
public class CheckoutStrategyFactory {

	public CheckoutStrategyFactory() {

	}

	public CheckoutStrategy createStrategy(Promotion promotion) {
		CheckoutStrategy checkoutStrategy;
		switch (promotion.getType()) {
		case BuyXGetYFree.TYPE:
			checkoutStrategy = new BuyXGetYFreeCheckout();
			break;
		case FlatPercent.TYPE:
			checkoutStrategy = new FlatPercentCheckout();
			break;
		case QuantityBasedPriceOverride.TYPE:
			checkoutStrategy = new QuantityBasedPriceOverrideCheckout();
			break;
		default:
			checkoutStrategy = new PlainCheckout();
		}
		return checkoutStrategy;
	}
}
