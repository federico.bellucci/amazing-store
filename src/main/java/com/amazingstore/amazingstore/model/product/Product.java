package com.amazingstore.amazingstore.model.product;

import java.util.Set;

import com.amazingstore.amazingstore.model.promotion.Promotion;

public class Product {
	protected String id;
	protected String name;
	protected int price;
	protected Set<Promotion> promotions;
	
	public Product() {
		
	}

	public Product(String id, String name, int price, Set<Promotion> promotions) {
		this.id = id;
		this.name = name;
		this.price = price;
		this.promotions = promotions;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the price
	 */
	public int getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(int price) {
		this.price = price;
	}

	/**
	 * @return the promotions
	 */
	public Set<Promotion> getPromotions() {
		return promotions;
	}

	/**
	 * @param promotions the promotions to set
	 */
	public void setPromotions(Set<Promotion> promotions) {
		this.promotions = promotions;
	}
}
