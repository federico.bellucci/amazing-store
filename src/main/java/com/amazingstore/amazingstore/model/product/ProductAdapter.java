package com.amazingstore.amazingstore.model.product;

import org.springframework.stereotype.Component;

@Component
public interface ProductAdapter<T> {
	
	Product getProduct(T object);
}
