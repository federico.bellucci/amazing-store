package com.amazingstore.amazingstore.model.promotion;

public abstract class AbstractPromotion implements Promotion {
	
	protected String id;
	protected String type;
	
	/**
	 * @return the id
	 */
	public String getId() {
		return this.id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the code
	 */
	public String getType() {
		return this.type;
	}

	/**
	 * @param type the code to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	
}
