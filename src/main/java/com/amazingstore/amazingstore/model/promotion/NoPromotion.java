package com.amazingstore.amazingstore.model.promotion;

public class NoPromotion extends AbstractPromotion {

	public static final String TYPE = "NO_PROMOTION";

	public NoPromotion() {
		this.id = "noPromotion";
		this.type = TYPE;
	}

	@Override
	public String getName() {
		return "No promotion";
	}

}
