package com.amazingstore.amazingstore.model.promotion;

public interface Promotion {
	
	/**
	 * @return the id
	 */
	public String getId();

	/**
	 * @param id the id to set
	 */
	public void setId(String id);

	/**
	 * @return the code
	 */
	public String getType();

	/**
	 * @param code the code to set
	 */
	public void setType(String code);
	
	/**
	 * 
	 * @return the computed name of the promotion
	 */
	public String getName();
	
}
