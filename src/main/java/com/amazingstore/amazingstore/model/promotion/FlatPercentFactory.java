package com.amazingstore.amazingstore.model.promotion;

import java.util.Map;

public class FlatPercentFactory implements AbstractPromotionFactory {

	public static final String FIELD_AMOUNT = "amount";

	@Override
	public Promotion createPromotion(Map<String, String> attributes) {
		return new FlatPercent(attributes.get(AbstractPromotionFactory.FIELD_ID),
				Integer.parseInt(attributes.get(FIELD_AMOUNT)));
	}

}
