package com.amazingstore.amazingstore.model.promotion;

public class FlatPercent extends AbstractPromotion {

	public static final String TYPE = "FLAT_PERCENT";

	protected int amount;

	public FlatPercent(String id, int amount) {
		this.id = id;
		this.type = TYPE;
		this.amount = amount;
	}

	/**
	 * @return the amount
	 */
	public int getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(int amount) {
		this.amount = amount;
	}

	@Override
	public String getName() {
		return String.format("%d%% discount", amount);

	}

}
