package com.amazingstore.amazingstore.model.promotion;

import java.util.Map;

public class BuyXGetYFreeFactory implements AbstractPromotionFactory {

	public static final String FIELD_REQUIRED_QTY = "required_qty";
	public static final String FIELD_FREE_QTY = "free_qty";

	@Override
	public Promotion createPromotion(Map<String, String> attributes) {
		return new BuyXGetYFree(attributes.get(AbstractPromotionFactory.FIELD_ID),
				Integer.parseInt(attributes.get(FIELD_REQUIRED_QTY)),
				Integer.parseInt(attributes.get(FIELD_FREE_QTY), 10));
	}

}
