package com.amazingstore.amazingstore.model.promotion;

import com.amazingstore.amazingstore.utils.Utils;

public class QuantityBasedPriceOverride extends AbstractPromotion {

	public static final String TYPE = "QTY_BASED_PRICE_OVERRIDE";

	protected int requiredQuantity;
	protected int price;

	public QuantityBasedPriceOverride(String id, int requiredQuantity, int price) {
		this.id = id;
		this.type = TYPE;
		this.requiredQuantity = requiredQuantity;
		this.price = price;
	}

	/**
	 * @return the requiredQuantity
	 */
	public int getRequiredQuantity() {
		return requiredQuantity;
	}

	/**
	 * @param requiredQuantity the requiredQuantity to set
	 */
	public void setRequiredQuantity(int requiredQuantity) {
		this.requiredQuantity = requiredQuantity;
	}

	/**
	 * @return the price
	 */
	public int getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(int price) {
		this.price = price;
	}

	@Override
	public String getName() {
		return String.format("Get %d, pay only %s", requiredQuantity, Utils.getPriceString(price));
	}
	
	

}
