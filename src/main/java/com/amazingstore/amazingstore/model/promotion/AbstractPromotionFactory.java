package com.amazingstore.amazingstore.model.promotion;

import java.util.Map;

public interface AbstractPromotionFactory {
	public static final String FIELD_ID = "id";
	public static final String FIELD_TYPE = "type";
	
	Promotion createPromotion(Map<String, String> attributes); 
}
