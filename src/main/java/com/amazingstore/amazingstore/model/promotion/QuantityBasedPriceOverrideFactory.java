package com.amazingstore.amazingstore.model.promotion;

import java.util.Map;

public class QuantityBasedPriceOverrideFactory implements AbstractPromotionFactory {

	public static final String FIELD_REQUIRED_QTY = "required_qty";
	public static final String FIELD_PRICE = "price";

	@Override
	public Promotion createPromotion(Map<String, String> attributes) {
		return new QuantityBasedPriceOverride(attributes.get(AbstractPromotionFactory.FIELD_ID),
				Integer.parseInt(attributes.get(FIELD_REQUIRED_QTY)),
				Integer.parseInt(attributes.get(FIELD_PRICE), 10));
	}

}
