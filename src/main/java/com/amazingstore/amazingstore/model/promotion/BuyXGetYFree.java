package com.amazingstore.amazingstore.model.promotion;

public class BuyXGetYFree extends AbstractPromotion {

	public static final String TYPE = "BUY_X_GET_Y_FREE";

	protected int requiredQuantity;
	protected int freeQuantity;

	public BuyXGetYFree(String id, int requiredQuantity, int freeQuantity) {
		this.id = id;
		this.type = TYPE;
		this.requiredQuantity = requiredQuantity;
		this.freeQuantity = freeQuantity;
	}

	/**
	 * @return the requiredQuantity
	 */
	public int getRequiredQuantity() {
		return requiredQuantity;
	}

	/**
	 * @param requiredQuantity the requiredQuantity to set
	 */
	public void setRequiredQuantity(int requiredQuantity) {
		this.requiredQuantity = requiredQuantity;
	}

	/**
	 * @return the freeQuantity
	 */
	public int getFreeQuantity() {
		return freeQuantity;
	}

	/**
	 * @param freeQuantity the freeQuantity to set
	 */
	public void setFreeQuantity(int freeQuantity) {
		this.freeQuantity = freeQuantity;
	}

	@Override
	public String getName() {
		return String.format("Buy %d, get %d free", requiredQuantity, freeQuantity);
	}

}
