package com.amazingstore.amazingstore.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.client.ExpectedCount.manyTimes;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.client.RequestMatcher;

import com.amazingstore.amazingstore.client.ProductPromotionTemplate;
import com.amazingstore.amazingstore.client.ProductTemplate;
import com.amazingstore.amazingstore.test.web.client.RequestContainsUriMatcher;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@RestClientTest(ProductTemplateServiceInterface.class)
public class ProductTemplateServiceTest {

	@Autowired
	protected ProductTemplateServiceInterface client;

	@Autowired
	protected MockRestServiceServer server;

	@Autowired
	protected ObjectMapper objectMapper;

	public RequestMatcher requestContainsUri(String uri) {
		return new RequestContainsUriMatcher(uri);
	}

	public void setUpProductPromotionCall() throws Exception {
		String productString = objectMapper.writeValueAsString(
				new ProductPromotionTemplate("productid", "name", 100, new HashSet<Map<String, String>>()));

		this.server.expect(requestTo(ProductTemplateServiceInterface.PRODUCT_API + "/productid"))
				.andRespond(withSuccess(productString, MediaType.APPLICATION_JSON));
	}

	public void setUpProductsCall() throws Exception {
		List<ProductTemplate> productTemplates = new ArrayList<ProductTemplate>();
		productTemplates.add(new ProductTemplate("id1", "name1", 100));
		productTemplates.add(new ProductTemplate("id2", "name2", 200));
		String productsString = objectMapper.writeValueAsString(productTemplates);

		this.server.expect(requestTo(ProductTemplateServiceInterface.PRODUCT_API))
				.andRespond(withSuccess(productsString, MediaType.APPLICATION_JSON));
	}

	public void setUpProductPromotionsCall() throws Exception {
		this.server.expect(manyTimes(), requestContainsUri(ProductTemplateServiceInterface.PRODUCT_API))
				.andRespond(withSuccess());
	}

	@Test
	public void whenCallingGetProducts_thenClientMakesCorrectCall() throws Exception {
		this.setUpProductsCall();

		List<ProductTemplate> productTemplates = this.client.getProductTemplates();

		assertEquals(2, productTemplates.size());
		assertEquals(100, productTemplates.get(0).getPrice());
		assertEquals(200, productTemplates.get(1).getPrice());
	}

	@Test
	public void whenCallingGetProductPromotion_thenClientMakesCorrectCall() throws Exception {
		this.setUpProductPromotionCall();
		ProductPromotionTemplate productPromotionTemplate = this.client.getProductPromotionTemplate("productid");

		assertEquals("name", productPromotionTemplate.getName());
		assertEquals(100, productPromotionTemplate.getPrice());
		assertTrue(productPromotionTemplate.getPromotions().isEmpty());
	}

	@Test
	public void whenCallingGetProductPromotionsIds_thenClientMakesCorrectCall() throws Exception {
		this.setUpProductPromotionsCall();
		List<String> ids = new ArrayList<String>();
		ids.add("id1");
		ids.add("id2");
		List<ProductPromotionTemplate> productPromotionTemplates = this.client.getProductPromotionTemplates(ids);
		assertEquals(2, productPromotionTemplates.size());
	}
}