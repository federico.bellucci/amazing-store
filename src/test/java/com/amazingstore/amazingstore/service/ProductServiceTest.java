package com.amazingstore.amazingstore.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.AdditionalMatchers.or;
import static org.mockito.Mockito.RETURNS_SMART_NULLS;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import com.amazingstore.amazingstore.model.checkout.Checkout;
import com.amazingstore.amazingstore.model.product.Product;
import com.amazingstore.amazingstore.model.promotion.Promotion;

public class ProductServiceTest {

	Product getMockProduct(String productId) {
		Product mock = mock(Product.class);
		mock.setId(productId);
		return mock;
	}

	@Test
	public void testCheckout() throws Exception {

		ProductService mockService = mock(ProductService.class, RETURNS_SMART_NULLS);

		when(mockService.checkout(any(), any())).thenCallRealMethod();

		doReturn(new Checkout(), new Checkout(), new Checkout()).when(mockService)
				.getSingleProductCheckout(any(Product.class), anyInt());

		// the next line throws nullPointerException
//		when(mockService.getSingleProductCheckout(any(), any())).thenReturn(new Checkout(), new Checkout(),	new Checkout());

		Set<Product> productSet = new HashSet<Product>();
		List<Product> checkoutProducts = new ArrayList<Product>();
		String[] chekoutProductIds = new String[] { "id1", "id2", "id1", "id3" };
		for (String productId : chekoutProductIds) {
			Product product = new Product();
			product.setId(productId);
			productSet.add(product);
			checkoutProducts.add(product);
		}

		mockService.checkout(productSet, chekoutProductIds);

		verify(mockService).getSingleProductCheckout(eq(checkoutProducts.get(1)), eq(1));
		verify(mockService).getSingleProductCheckout(or(eq(checkoutProducts.get(0)), eq(checkoutProducts.get(2))),
				eq(2));
		verify(mockService).getSingleProductCheckout(eq(checkoutProducts.get(3)), eq(1));
	}

	@Test
	public void whenGetSingleProductCheckoutWithNoAmount_returnsEmpty() throws Exception {
		ProductService productService = new ProductService();
		Checkout checkout = productService.getSingleProductCheckout(null, 0);
		assertEquals(checkout.getTotal(), 0);
		assertEquals(checkout.getPromo(), 0);
		assertEquals(checkout.getPayable(), 0);
	}

	@Test(expected = NullPointerException.class)
	public void whenGetSingleProductCheckoutWithNullProduct_throws() throws Exception {
		ProductService productService = new ProductService();
		productService.getSingleProductCheckout(null, 1);
	}
	
	@Test
	public void whenGetSingleProductCheckoutWithNoPromotions_returnsPlainCheckout() throws Exception {
		ProductService productService = new ProductService();
		Product product = new Product();
		product.setPrice(100);
		Set<Promotion> promotions = new HashSet<Promotion>();
		product.setPromotions(promotions);
		Checkout checkout = productService.getSingleProductCheckout(product, 5);
		assertEquals(500, checkout.getTotal());
	}
	
}