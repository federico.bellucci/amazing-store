package com.amazingstore.amazingstore.utils;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class UtilsTest {

	@Test
	public void testGetPriceString () {
		assertEquals("£0.01", Utils.getPriceString(1));
		assertEquals("£0.19", Utils.getPriceString(19));
		assertEquals("£1.99", Utils.getPriceString(199));
		assertEquals("£19.90", Utils.getPriceString(1990));
		assertEquals("£199.00", Utils.getPriceString(19900));
	}
}
