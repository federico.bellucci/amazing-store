package com.amazingstore.amazingstore.client;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

import com.amazingstore.amazingstore.model.product.Product;
import com.amazingstore.amazingstore.model.product.ProductAdapter;
import com.amazingstore.amazingstore.model.promotion.AbstractPromotionFactory;
import com.amazingstore.amazingstore.model.promotion.BuyXGetYFree;
import com.amazingstore.amazingstore.model.promotion.BuyXGetYFreeFactory;
import com.amazingstore.amazingstore.model.promotion.FlatPercent;
import com.amazingstore.amazingstore.model.promotion.FlatPercentFactory;
import com.amazingstore.amazingstore.model.promotion.Promotion;
import com.amazingstore.amazingstore.model.promotion.QuantityBasedPriceOverride;
import com.amazingstore.amazingstore.model.promotion.QuantityBasedPriceOverrideFactory;

public class ProductTemplateAdapterTest {
	
	public ProductPromotionTemplate getProductTemplate(Set<Map<String,String>> promotionMaps) {
		ProductPromotionTemplate productTemplate = new ProductPromotionTemplate();
		productTemplate.setId("id");
		productTemplate.setName("name");
		productTemplate.setPrice(199);
		productTemplate.setPromotions(promotionMaps);
		
		return productTemplate;
	}
	
	public Map<String,String> getUnhandledPromotionMap () {
		Map<String, String> promotion = new HashMap<String, String>();
		promotion.put(AbstractPromotionFactory.FIELD_ID, "pid");
		promotion.put(AbstractPromotionFactory.FIELD_TYPE, "UNKNOWN");
		promotion.put("new_field", "any");
		return promotion;
	}
	
	public Map<String,String> getBuyXGetYFreeMap () {
		Map<String, String> promotion = new HashMap<String, String>();
		promotion.put(AbstractPromotionFactory.FIELD_ID, "pid");
		promotion.put(AbstractPromotionFactory.FIELD_TYPE, BuyXGetYFree.TYPE);
		promotion.put(BuyXGetYFreeFactory.FIELD_REQUIRED_QTY, "2");
		promotion.put(BuyXGetYFreeFactory.FIELD_FREE_QTY, "2");
		return promotion;
	}
	
	public Map<String,String> getFlatPercentMap () {
		Map<String, String> promotion = new HashMap<String, String>();
		promotion.put(AbstractPromotionFactory.FIELD_ID, "pid");
		promotion.put(AbstractPromotionFactory.FIELD_TYPE, FlatPercent.TYPE);
		promotion.put(FlatPercentFactory.FIELD_AMOUNT, "10");
		return promotion;
	}
	
	public Map<String,String> getQuantityBasedPriceOverrideMap () {
		Map<String, String> promotion = new HashMap<String, String>();
		promotion.put(AbstractPromotionFactory.FIELD_ID, "pid");
		promotion.put(AbstractPromotionFactory.FIELD_TYPE, QuantityBasedPriceOverride.TYPE);
		promotion.put(QuantityBasedPriceOverrideFactory.FIELD_REQUIRED_QTY, "2");
		promotion.put(QuantityBasedPriceOverrideFactory.FIELD_PRICE, "199");
		return promotion;
	}
	
//	@Test
//	public void testSame() {
//		ProductTemplate productTemplate = this.getProductTemplate();
//		
//		Set<Promotion> expectedPromotions = new HashSet<Promotion>();
//		Promotion expectedPromotion = new BuyXGetYFree("pid", 2, 1);
//		expectedPromotions.add(expectedPromotion);
//		Product expected = new Product("id", "name", 199, expectedPromotions); 
//		
//		ProductAdapter<ProductTemplate> adapter = new ProductTemplateAdapter();
//		
//		Product product = adapter.getProduct(productTemplate);
//		
//		assertSame(product, expected);
//	}
	
	@Test
	public void testUnhandledAdapter() {
		Set<Map<String, String>> promotions = new HashSet<Map<String, String>>(); 
		promotions.add(this.getUnhandledPromotionMap());
		ProductPromotionTemplate productTemplate = this.getProductTemplate(promotions);
		
		ProductAdapter<ProductPromotionTemplate> adapter = new ProductTemplateAdapter();
		Product product = adapter.getProduct(productTemplate);
		
		assertEquals(product.getPromotions().toArray()[0], null);
	}
	
	@Test
	public void testBuyXGetYFreeAdapter() {
		Set<Map<String, String>> promotions = new HashSet<Map<String, String>>(); 
		promotions.add(this.getBuyXGetYFreeMap());
		ProductPromotionTemplate productTemplate = this.getProductTemplate(promotions);
		
		Promotion expectedPromotion = new BuyXGetYFree("pid", 2, 1);
		ProductAdapter<ProductPromotionTemplate> adapter = new ProductTemplateAdapter();
		Product product = adapter.getProduct(productTemplate);
		
		assertEquals(product.getPromotions().toArray()[0].getClass(), expectedPromotion.getClass());
	}
	
	@Test
	public void testFlatPercentAdapter() {
		Set<Map<String, String>> promotions = new HashSet<Map<String, String>>(); 
		promotions.add(this.getFlatPercentMap());
		ProductPromotionTemplate productTemplate = this.getProductTemplate(promotions);
		
		Promotion expectedPromotion = new FlatPercent("pid", 10);
		ProductAdapter<ProductPromotionTemplate> adapter = new ProductTemplateAdapter();
		Product product = adapter.getProduct(productTemplate);
		
		assertEquals(product.getPromotions().toArray()[0].getClass(), expectedPromotion.getClass());
	}
	
	@Test
	public void testQuantityBasedPriceOverrideAdapter() {
		Set<Map<String, String>> promotions = new HashSet<Map<String, String>>(); 
		promotions.add(this.getQuantityBasedPriceOverrideMap());
		ProductPromotionTemplate productTemplate = this.getProductTemplate(promotions);
		
		Promotion expectedPromotion = new QuantityBasedPriceOverride("pid", 2, 199);
		ProductAdapter<ProductPromotionTemplate> adapter = new ProductTemplateAdapter();
		Product product = adapter.getProduct(productTemplate);
		
		assertEquals(product.getPromotions().toArray()[0].getClass(), expectedPromotion.getClass());
	}

}
