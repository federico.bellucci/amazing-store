package com.amazingstore.amazingstore.model;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.amazingstore.amazingstore.model.checkout.Checkout;
import com.amazingstore.amazingstore.model.checkout.CheckoutStrategy;
import com.amazingstore.amazingstore.model.checkout.FlatPercentCheckout;
import com.amazingstore.amazingstore.model.promotion.FlatPercent;
import com.amazingstore.amazingstore.model.promotion.Promotion;

@RunWith(Parameterized.class)
public class FlatPercentCheckoutTest {
	static final int PRICE = 100;
	CheckoutStrategy strategy;
	int amount;
	int percentAmount;
	int expectedPromo;

	@Before
	public void initialize() {
		strategy = new FlatPercentCheckout();
	}

	public FlatPercentCheckoutTest(int amount, int percentAmount, int expectedPromo) {
		this.strategy = new FlatPercentCheckout();
		this.amount = amount;
		this.percentAmount = percentAmount;
		this.expectedPromo = expectedPromo;
	}

	@Parameterized.Parameters
	public static Collection<Integer[]> expectedPromos() {
		return Arrays.asList(new Integer[][] { { 2, 10, 20 }, { 2, 0, 0 }, { 2, 100, 200 } });
	}

	@Test
	public void testFlatPercent() throws Exception {
		Promotion promotion = new FlatPercent("id", percentAmount);
		Checkout checkout = strategy.checkout(promotion, PRICE, amount);
		assertEquals(expectedPromo, checkout.getPromo());
	}
}
