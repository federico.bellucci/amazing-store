package com.amazingstore.amazingstore.model;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.amazingstore.amazingstore.model.checkout.Checkout;
import com.amazingstore.amazingstore.model.checkout.CheckoutStrategy;
import com.amazingstore.amazingstore.model.checkout.QuantityBasedPriceOverrideCheckout;
import com.amazingstore.amazingstore.model.promotion.Promotion;
import com.amazingstore.amazingstore.model.promotion.QuantityBasedPriceOverride;

@RunWith(Parameterized.class)
public class QuantityBasedPriceOverrideCheckoutTest {
	static final int PRICE = 100;
	CheckoutStrategy strategy;
	int amount;
	int requiredQuantity;
	int overridePrice;
	int expectedPayable;

	@Before
	public void initialize() {
		strategy = new QuantityBasedPriceOverrideCheckout();
	}

	public QuantityBasedPriceOverrideCheckoutTest(int amount, int requiredQuantity, int overridePrice,
			int expectedPayable) {
		this.amount = amount;
		this.requiredQuantity = requiredQuantity;
		this.overridePrice = overridePrice;
		this.expectedPayable = expectedPayable;
	}

	@Parameterized.Parameters
	public static Collection<Integer[]> expectedPromos() {
		return Arrays.asList(new Integer[][] { { 2, 2, 150, 150 }, { 4, 2, 150, 2 * 150 }, { 3, 2, 150, 150 + PRICE },
				{ 3, 2, 0, PRICE }, { 3, 1, 100, 3 * 100 }, { 3, 3, 150, 150 }, { 2, 3, 150, 2 * PRICE } });
	}

	@Test
	public void testQuantityBasedPriceOverride() throws Exception {
		Promotion promotion = new QuantityBasedPriceOverride("id", requiredQuantity, overridePrice);
		Checkout checkout = strategy.checkout(promotion, PRICE, amount);
		assertEquals(expectedPayable, checkout.getPayable());
	}
}
