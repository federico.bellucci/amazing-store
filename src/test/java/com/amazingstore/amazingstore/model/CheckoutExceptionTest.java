package com.amazingstore.amazingstore.model;

import org.junit.Test;

import com.amazingstore.amazingstore.model.checkout.BuyXGetYFreeCheckout;
import com.amazingstore.amazingstore.model.checkout.CheckoutStrategy;
import com.amazingstore.amazingstore.model.checkout.FlatPercentCheckout;
import com.amazingstore.amazingstore.model.checkout.QuantityBasedPriceOverrideCheckout;
import com.amazingstore.amazingstore.model.promotion.BuyXGetYFree;
import com.amazingstore.amazingstore.model.promotion.FlatPercent;
import com.amazingstore.amazingstore.model.promotion.Promotion;
import com.amazingstore.amazingstore.model.promotion.QuantityBasedPriceOverride;

public class CheckoutExceptionTest {

	@Test(expected = Exception.class)
	public void whenCheckoutBuyXGetYFreeWithRequiredZero_throwsException() throws Exception {
		CheckoutStrategy strategy = new BuyXGetYFreeCheckout();
		Promotion promotion = new BuyXGetYFree("id", 0, 1);
		strategy.checkout(promotion, 100, 1);
	}
	
	@Test(expected = Exception.class)
	public void whenCheckoutBuyXGetYFreeWithNegativeFree_throwsException() throws Exception {
		CheckoutStrategy strategy = new BuyXGetYFreeCheckout();
		Promotion promotion = new BuyXGetYFree("id", 1, -1);
		strategy.checkout(promotion, 100, 1);
	}
	
	@Test(expected = Exception.class)
	public void whenCheckoutBuyXGetYFreeWithFreeGreaterThanRequired_throwsException() throws Exception {
		CheckoutStrategy strategy = new BuyXGetYFreeCheckout();
		Promotion promotion = new BuyXGetYFree("id", 1, 2);
		strategy.checkout(promotion, 100, 1);
	}
	
	@Test(expected = Exception.class)
	public void whenCheckoutFlatPercentWith110Percent_throwsException() throws Exception {
		CheckoutStrategy strategy = new FlatPercentCheckout();
		Promotion promotion = new FlatPercent("id", 110);
		strategy.checkout(promotion, 100, 1);
	}
	
	@Test(expected = Exception.class)
	public void whenCheckoutFlatPercentWithNegativePercent_throwsException() throws Exception {
		CheckoutStrategy strategy = new FlatPercentCheckout();
		Promotion promotion = new FlatPercent("id", -10);
		strategy.checkout(promotion, 100, 1);
	}
	
	@Test(expected = Exception.class)
	public void whenCheckoutQuantityBasedPriceOverrideWithZeroRequiredthrowsException() throws Exception {
		CheckoutStrategy strategy = new QuantityBasedPriceOverrideCheckout();
		Promotion promotion = new QuantityBasedPriceOverride("id", 0, 100);
		strategy.checkout(promotion, 100, 1);
	}
	
	@Test(expected = Exception.class)
	public void whenCheckoutQuantityBasedPriceOverrideWithNegativePricethrowsException() throws Exception {
		CheckoutStrategy strategy = new QuantityBasedPriceOverrideCheckout();
		Promotion promotion = new QuantityBasedPriceOverride("id", 1, -10);
		strategy.checkout(promotion, 100, 1);
	}
}
