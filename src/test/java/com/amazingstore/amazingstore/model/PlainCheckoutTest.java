package com.amazingstore.amazingstore.model;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.amazingstore.amazingstore.model.checkout.Checkout;
import com.amazingstore.amazingstore.model.checkout.CheckoutStrategy;
import com.amazingstore.amazingstore.model.checkout.PlainCheckout;
import com.amazingstore.amazingstore.model.promotion.NoPromotion;
import com.amazingstore.amazingstore.model.promotion.Promotion;

@RunWith(Parameterized.class)
public class PlainCheckoutTest {
	Promotion promotion;
	int price;
	int amount;
	CheckoutStrategy strategy;
	int expectedPromo;

	@Before
	public void initialize() {
		strategy = new PlainCheckout();
		promotion = new NoPromotion();
	}

	public PlainCheckoutTest(int price, int amount, int expectedPromo) {
		this.promotion = new NoPromotion();
		this.strategy = new PlainCheckout();
		this.amount = amount;
		this.price = price;
		this.expectedPromo = expectedPromo;
	}

	@Parameterized.Parameters
	public static Collection<Object[]> expectedPromos() {
		return Arrays.asList(new Object[][] { { 100, 5, 0 }, { 0, 5, 0 }, { 0, 0, 0 } });
	}

	@Test
	public void testPlainCheckout () throws Exception {
		Checkout checkout = strategy.checkout(promotion, price, amount);
		assertEquals(expectedPromo, checkout.getPromo());
	}
}
