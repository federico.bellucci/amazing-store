package com.amazingstore.amazingstore.model;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.amazingstore.amazingstore.model.checkout.BuyXGetYFreeCheckout;
import com.amazingstore.amazingstore.model.checkout.Checkout;
import com.amazingstore.amazingstore.model.checkout.CheckoutStrategy;
import com.amazingstore.amazingstore.model.checkout.PlainCheckout;
import com.amazingstore.amazingstore.model.promotion.BuyXGetYFree;

@RunWith(Parameterized.class)
public class BuyXGetYFreeCheckoutTest {
	static final int PRICE = 100;
	CheckoutStrategy strategy;
	int amount;
	int requiredQuantity;
	int freeQuantity;
	int expectedPromo;

	@Before
	public void initialize() {
		strategy = new BuyXGetYFreeCheckout();
	}

	public BuyXGetYFreeCheckoutTest(int amount, int requiredQuantity, int freeQuantity, int expectedPromo) {
		this.strategy = new PlainCheckout();
		this.amount = amount;
		this.requiredQuantity = requiredQuantity;
		this.freeQuantity = freeQuantity;
		this.expectedPromo = expectedPromo;
	}

	@Parameterized.Parameters
	public static Collection<Integer[]> expectedPromos() {
		return Arrays.asList(new Integer[][] { { 2, 2, 1, 100 }, { 0, 2, 1, 0 }, { 2, 1, 0, 0 }, { 3, 2, 1, 100 },
				{ 3, 3, 1, 100 }, { 2, 3, 1, 0 } });
	}

	@Test
	public void testBuyXGetYFree() throws Exception {
		BuyXGetYFree promotion = new BuyXGetYFree("id", requiredQuantity, freeQuantity);
		Checkout checkout = strategy.checkout(promotion, PRICE, amount);
		assertEquals(expectedPromo, checkout.getPromo());
	}
}
