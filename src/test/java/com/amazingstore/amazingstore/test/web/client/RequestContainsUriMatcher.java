package com.amazingstore.amazingstore.test.web.client;


import static org.junit.Assert.assertTrue;

import java.io.IOException;
import org.springframework.test.web.client.RequestMatcher;
import org.springframework.http.client.ClientHttpRequest;

public class RequestContainsUriMatcher implements RequestMatcher {
    private final String uri;

    public RequestContainsUriMatcher(String uri){
        this.uri = uri;
    }

    @Override
    public void match(ClientHttpRequest clientHttpRequest) throws IOException, AssertionError {
        assertTrue(clientHttpRequest.getURI().toString().contains(uri));
    }
}
